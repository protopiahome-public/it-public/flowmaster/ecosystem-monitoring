var jose = require('node-jose');

// sign
// *  header: the combined 'protected' and 'unprotected' header members
// *  payload: Buffer of the signed content
// *  signature: Buffer of the verified signature
// *  key: The key used to verify the signature

//crypto
// *  header: the combined 'protected' and 'unprotected' header members
// *  protected: an array of the member names from the "protected" member
// *  key: Key used to decrypt
// *  payload: Buffer of the decrypted content
// *  plaintext: Buffer of the decrypted content (alternate)
export default class TokenType {

    token = "";
    lifetime = "";

    /**  header:  The combined 'protected' and 'unprotected' header members */
    header ="";
    /**  payload: Buffer of the signed content or decrypted content; In command "sign" {input} is this Buffer */
    payload ="";
    /**  signature: Buffer of the verified signature */
    signature ="";
    /**  key || keystore: Key used to decrypt key or verify the signature */
    key ="";
    /**  protected: An array of the member names from the "protected" member */
    protected ="";
    /** plaintext : Buffer of the decrypted content (alternate) */
    plaintext ="";



    /**
     * Generate
     *
     * @param token token string in compact form
     * @param key key or keystore
     */
    constructor(token ="", key) {
        this.token = token;
        this.key = key;
        // createKey
    }

    /**
     * set Key
     *
     * @param key key or keystore
     */
    setKey(key){
        this.key = key;
    }

    /**
     * set Token
     *
     * @param token key or keystore
     */
    setToken(token){
        this.token = token;
    }

    getPayloadJSON(){
        return JSON.parse(this.payload.toString());
    }

    getKey(){
        return this.key;
    }

    getKeyJSON(isPrivate = false){
        // t.key.toJSON(true);
        return this.key.toJSON(isPrivate);
    }

    expires_in (){
        const json = JSON.parse(this.payload.toString());
        const exp = json.exp;
        return exp;
    }

    async encrypt  (){
        const buffer = this.payload;

        this.token = await jose.JWE.createEncrypt({ format: 'compact' }, this.key)
        .update(buffer).
        final();

        return this.token;
    }

    async decrypt (){
        const json = await jose.JWE.createDecrypt(this.key).decrypt(this.token)

        this.header = json.header;
        this.payload =json.payload;
        this.plaintext =json.plaintext;
        this.protected =json.protected;
        this.key =json.key;

        return json;
    }

    async sign (){
        const buffer = this.payload;

        this.token = await jose.JWS.createSign({ format: 'compact' }, this.key).
        update(buffer).
        final();

        return this.token;
    }

    async verify (){
        const json = await jose.JWS.createVerify(this.key).
        verify(this.token);

        this.header = json.header;
        this.payload =json.payload;
        this.signature =json.signature;
        this.key =json.key;

        return json;

    }

}