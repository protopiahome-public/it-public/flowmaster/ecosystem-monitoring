//https://www.npmjs.com/package/mongoist
import config from "../config/db_config";

let mongojs = require('mongojs');
const mongoist = require('mongoist');

let userString = "";

if(config.user && config.password){
    userString = `${config.user}:${config.password}@`
}

const connectionString = `mongodb://${userString}${config.host}:${config.port}/${config.database}`;

//_config = {
//   "host" : "",
//   "user" : "",
//   "password" : "",
//   "database" : ""
// }

let db = mongoist(mongojs(connectionString, [
    "user",
    // "team",
    // "community",
    // "vk_group",
    // "tg_chat",
    // "event",
    // "person",
    // "place",
    // "proposal",
    // "resource",
    // "role",
    // "tool",
    // "project",
    "client"
]));

export default db;


