import gql from "graphql-tag";
import {readFileSync} from "fs";

import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import fetch from 'node-fetch'

const sendCode = readFileSync(__dirname + '/graphql/sendCode.graphql', 'utf8');

export default async function(msg, ctx) {
	try {
		const api_uri = msg.api_uri;

		const httpLink = createHttpLink({
			"uri": api_uri,
			fetch: fetch
		});

		console.log(api_uri);
		console.log(msg);

	// const authLink = setContext((_, { headers }) => {
	//     // get the authentication token from local storage if it exists
	//     // const token = localStorage.getItem('token');
	//     const token = "";
	//     // return the headers to the context so httpLink can read them
	//     // const xxx = base64_encode( login +':'+ password );
	//     //console.log(token);
	//     return {
	//         headers: {
	//             ...headers,
	//             authorization: token ? `Barier ${token}` : "",
	//             // authorization: 'Basic ' + xxx,
	//         }
	//     }
	// });



		const client = new ApolloClient({
			link: ApolloLink.from([
				// authLink,
				httpLink,
			]),
			cache: new InMemoryCache(),
			// defaultHttpLink: false,
			// cache: cache,

			// fetchOptions: {
			//     credentials: 'include'
			// }
		});

		const mutation = gql`
			${sendCode}
		`;

		client.mutate({
            mutation: mutation,
			variables: {
				external_type: msg.external_type,
				external_id: msg.external_id,
				message: msg.message
			},
		})
			.then(data => console.log(data))
			.catch(error => console.error(error));
	} catch (e) {
        console.log(e);
    }
}
