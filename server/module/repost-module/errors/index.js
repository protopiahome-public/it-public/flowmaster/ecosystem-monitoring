// https://www.apollographql.com/docs/apollo-server/features/errors/

const {ApolloError} = require('apollo-server');

export class InvalidInviteError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_INVITE');

        Object.defineProperty(this, 'name', {value: 'InvalidInviteError'});
    }
}

export class DoubleKeywordError extends ApolloError {
    constructor(message: string) {
        super(message, 'DOUBLE_KEYWORD');

        Object.defineProperty(this, 'name', {value: 'DoubleKeywordError'});
    }
}