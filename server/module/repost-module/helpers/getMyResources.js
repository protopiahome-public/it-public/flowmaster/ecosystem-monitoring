import {ObjectId} from "promised-mongo";

module.exports = {
	getMyResourcesFilter: async function(ctx, resource_name, condition) {
		let field_name = resource_name + "s_ids";
		let resource_ids = [];
		let receivers = await ctx.db.receiver.find({admin_ids: new ObjectId(ctx.user._id)})

		if (receivers) 
			receivers.forEach(function(el){if (el[field_name]) resource_ids = resource_ids.concat(el[field_name])});
		let result = {"$or": 
				[{_id: {"$in" : resource_ids}},
				{is_public : true}]
			};
		if (condition) {
			result = {"$and" : [result, condition]}
		};
		return result;
	}
}