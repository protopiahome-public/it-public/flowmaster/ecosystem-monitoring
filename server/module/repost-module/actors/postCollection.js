import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx)
{

    let groups = [];
    let match;

    const user_id = new ObjectId(msg.search.user_id);
	
	match = msg.search;
	
	if (match._id) {
		match._id = new ObjectId(match._id);
	}
	
	try {
    groups = await db.post.aggregate(
        [
            {"$match": match},
			{"$sort": {post_time: -1}}
        ]);
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  groups, ctx.self);

}