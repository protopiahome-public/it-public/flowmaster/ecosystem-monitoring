import authenticator from "otplib/authenticator";
import crypto from "crypto";
import db from "../../../db";
import { ObjectId } from "promised-mongo";

authenticator.options = { crypto };

const { dispatch } = require("nact");

const secret = "KVKFKRCPNZQUYMDSDFFEEFELXOVYDSQKJKZDTSRLD";

export default async function(msg, ctx) {
  let item;

  if (msg.input) {
    const invite_code = authenticator.generate(secret);

    await db.invite.insert({
      receiver_id: new ObjectId(msg.input.receiver_id),
      invite_code: invite_code,
      create_time: new Date()
    });

    dispatch(ctx.sender, invite_code, ctx.self);
  } else if (msg.search) {
    var d = new Date();
    d.setHours(d.getHours() - 24);
    msg.search.create_time = { $gt: d };

    item = await db.invite.findOne(msg.search);
    await db.invite.remove(msg.search);
    dispatch(ctx.sender, item, ctx.self);
  } else if (msg.show) {
    var d = new Date();
    d.setHours(d.getHours() - 24);
    msg.show.create_time = { $gt: d };

    item = await db.invite.findOne(msg.show);
    dispatch(ctx.sender, item, ctx.self);
  } else if (msg.delete) {
    var d = new Date();
    d.setHours(d.getHours() - 24);
    msg.delete.create_time = { $gt: d };

    item = await db.invite.findOne(msg.delete);
    await db.invite.remove(msg.delete);
    dispatch(ctx.sender, item, ctx.self);
  }
}
