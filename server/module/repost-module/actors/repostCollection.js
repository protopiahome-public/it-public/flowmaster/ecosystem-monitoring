import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx)
{

    let groups = [];
    let match;

    const user_id = new ObjectId(msg.search.user_id);
	
	match = msg.search;
	
	try {
    groups = await db.repost.aggregate(
        [
            {"$match": match},
            {
                "$lookup": {
                    "from":         "post",
                    "localField":   "post_id",
                    "foreignField": "_id",
                    "as":           "post"
                }
            },
			{
				"$unwind" : "$post"
			},
			{"$sort": {repost_time: -1}}
        ]);
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  groups, ctx.self);

}