import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx)
{

    let groups = [];
    let match;

    const user_id = new ObjectId(msg.search.user_id);
	delete msg.search.user_id;
	
    if(msg.search._id){
        match = {"$and": [
                {"$or": [{admin_ids: {"$in": [user_id]}}, {is_public: true}]},
                {"_id":  new ObjectId(msg.search._id) }
            ]}
    }else{
        match = {"$and": [
					msg.search, 
					{"$or": [
						{admin_ids: {"$in": [user_id]}}, 
						{is_public: true}
					]}
				]}
    }

	try {
    groups = await db.receiver.aggregate(
        [
            {"$match": match},
            {
                "$lookup": {
                    "from":         "theme",
                    "localField":   "themes_ids",
                    "foreignField": "_id",
                    "as":           "themes"
                }
            },
            {
                "$lookup": {
                    "from":         "source",
                    "localField":   "activate_source_ids",
                    "foreignField": "_id",
                    "as":           "activate_sources"
                }
            },
            {
                "$lookup": {
                    "from":         "user",
                    "localField":   "admin_ids",
                    "foreignField": "_id",
                    "as":           "admins"
                }
            }
            // , {"$limit": 1}
        ]);
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  groups, ctx.self);

}