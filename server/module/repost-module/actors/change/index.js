import gql from "graphql-tag";
import {readFileSync} from "fs";


const change = readFileSync(__dirname + '/graphql/changeBotToken.graphql', 'utf8');


import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import fetch from 'node-fetch'

const uri = "";
//telegram_bot uri;

// import {uri} from "../../../../config/telegram_bot";


//client.name
//client.uri
//client.send(uri).command()

const httpLink = createHttpLink({
    "uri": uri,
    fetch: fetch
});


const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    // const token = localStorage.getItem('token');
    const token = "";
    // return the headers to the context so httpLink can read them
    // const xxx = base64_encode( login +':'+ password );
    //console.log(token);
    return {
        headers: {
            ...headers,
            authorization: token ? `Barier ${token}` : "",
            // authorization: 'Basic ' + xxx,
        }
    }
});

//const stateLink = withClientState({
//     resolvers,
//     defaults,
//     cache,
//     typeDefs
// });



const client = new ApolloClient({
    link: ApolloLink.from([
        authLink,
        httpLink,
    ]),
    cache: new InMemoryCache(),
    // defaultHttpLink: false,
    // cache: cache,

    // fetchOptions: {
    //     credentials: 'include'
    // }
});



export default async function(msg, ctx) {

    const mutate = gql`        
        ${change}
    `;

    client.mutate({
        mutate: mutate,
        variables: {
            "bot_token": msg.bot_token
        },
    })
        .then(data => console.log(data))
        .catch(error => console.error(error));

}

// changeBotToken (botToken: String): String mutation