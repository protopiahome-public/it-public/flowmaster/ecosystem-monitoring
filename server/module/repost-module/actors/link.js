import authenticator from 'otplib/authenticator';
import crypto from 'crypto';
import db from "../../../db";
import {ObjectId} from "promised-mongo";

authenticator.options = { crypto };

const { dispatch } = require('nact');

const secret = 'KVKFKRCPNZQUYMDSDFFEEFELXOVYDSQKJKZDTSRLD';

export default async function(msg, ctx) {


    let item;

    if(msg.input){
        const link_code = authenticator.generate(secret);

        await db.link_associate.insert({
            "link_id": msg.input._id,
            "link_code": link_code
        });

        dispatch(ctx.sender,   link_code , ctx.self);
    }else if (msg.search){
        item = await db.link_associate.findOne(msg.search);

        // try {
        //     return authenticator.verify({token, secret});
        // } catch (err) {
        //
        //     console.error(err);
        // }
		dispatch(ctx.sender,   item.link_id , ctx.self);
    }


}





