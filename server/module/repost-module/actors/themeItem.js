import db from "../../../db";
import themeCollection from "./themeCollection";

import {ObjectId} from "promised-mongo";
import {getMyResourcesFilter} from "../helpers/getMyResources";

const { query, dispatch } = require('nact');

//Stateless - Сохраняющий свое состояние во внешний источник.
//https://www.npmjs.com/package/flux-standard-action
//https://nact.io/en_uk/lesson/javascript/hierarchy

/**
 * Actor get one item
 *
 * @param msg.type collection type
 * @param msg.search search object
 * @param msg.input input object
 *
 * @return item item of collection
 */
export default async function(msg, ctx) {

    const receiverCollectionActor = ctx.children.get("receivers");

    let item;
console.log(msg);
    if(msg.search && msg.input){

        if(msg.search._id){
            msg.search._id = new ObjectId(msg.search._id);
            const user_id = new ObjectId(msg.user_id);

            let receivers = await query(receiverCollectionActor, {search: { user_id: user_id}}, 2000);
        }

		delete msg.search.user_id;
        item = await db.theme.findAndModify({
            query: msg.search,
            update: {
                // $set : db_args
                $set : msg.input
            }
        });

    }
	else if(msg.search && msg.full_input){

        if(msg.search._id){
            msg.search._id = new ObjectId(msg.search._id);
            const user_id = new ObjectId(msg.search.user_id);

            let receivers = await query(receiverCollectionActor, {search: { user_id: user_id}}, 2000);
        }
		delete msg.search.user_id;
        item = await db.theme.findAndModify({
            query: msg.search,
            update: msg.full_input
		});

    }
	else if(msg.input){
        item = await db.theme.insert(msg.input);
    }else if (msg.search){
        if(msg.search._id){
            msg.search._id = new ObjectId(msg.search._id);
            const user_id = new ObjectId(msg.search.user_id);

            let receivers = await query(receiverCollectionActor, {search: { user_id: user_id}}, 2000);
        }

        item = await db.theme.findOne(msg.search);

    }

    dispatch(ctx.sender,   item , ctx.self);

}