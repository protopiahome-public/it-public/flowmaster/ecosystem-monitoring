import {ObjectId} from "promised-mongo";
import db from "../../../db";

const { query, dispatch } = require('nact');

export default async function(msg, ctx) {

    let themes = [];
try {


     const receiverCollectionActor = ctx.children.get("receivers");

     const user_id = new ObjectId(msg.search.user_id);

     let receivers = await query(receiverCollectionActor, {search: { user_id: user_id}}, 2000);
    
     receivers.forEach(function (el) {
         if (el['themes']) {
			 el.themes.forEach (function (theme) {
				theme.receiver = el; 
			 });
             themes = themes.concat(el['themes']);
			 el.themes = null;
		 }
     });

    let public_themes = [];
    try {
        public_themes = await db.theme.aggregate([{"$match":  {"is_public": true} }],
		{"$match": match},
                {
                    "$lookup": {
                        "from":         "receiver",
                        "localField":   "_id",
                        "foreignField": "themes_ids",
                        "as":           "receiver"
                    }
                }
			);
    }
    catch (e) {console.log(e);}

     // public_themes.forEach(function (el) {
     //     themes = themes.push(el);
     // });

    themes = themes.concat(public_themes);

	if (msg.search.id) {
		themes = themes.filter(function(theme) {
			return theme._id == msg.search.id;
		});
	}

} catch (e) {console.log(e)}


    dispatch(ctx.sender,  themes, ctx.self);
}