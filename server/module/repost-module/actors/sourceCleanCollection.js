import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx) {


    let groups = [];
    let match;

	match = msg.search;

	try {
        groups = await db.source.aggregate(
            [
                {"$match": match},
                {
                    "$lookup": {
                        "from":         "receiver",
                        "localField":   "shared_to_receivers_ids",
                        "foreignField": "_id",
                        "as":           "shared_to_receivers"
                    }
                },
                {
                    "$lookup": {
                        "from":         "user",
                        "localField":   "admin_ids",
                        "foreignField": "_id",
                        "as":           "admins"
                    }
                }
                // , {"$limit": 1}
            ]);
    } catch (e) {console.log(e);}

console.log(groups);

    dispatch(ctx.sender,  groups, ctx.self);

}