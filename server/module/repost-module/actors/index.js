import addPost from "./addPost";
import repostCollection from "./repostCollection";
import postCollection from "./postCollection";

import receiverCollection from "./receiverCollection";
import receiverCleanCollection from "./receiverCleanCollection";
import client from "./client";

import sourceCollection from "./sourceCollection";
import sourceCleanCollection from "./sourceCleanCollection";

import themeCollection from "./themeCollection";
import themeItem from "./themeItem";

import link from "./link";
import invite from "./invite";
import say from "./say";

const { spawnStateless} = require('nact');


export default function(serverActor){
    spawnStateless(serverActor, receiverCollection, "receivers");
    spawnStateless(serverActor, sourceCollection, "sources");
	spawnStateless(serverActor, repostCollection, "reposts");
	spawnStateless(serverActor, postCollection, "posts");

    const themeCollectionActor = spawnStateless(serverActor, themeCollection, "themes");
    spawnStateless(themeCollectionActor, receiverCollection, "receivers");

    const themeActor  = spawnStateless(serverActor, themeItem, "theme");
    spawnStateless(themeActor, receiverCollection, "receivers");

    spawnStateless(serverActor, link, "link");
	spawnStateless(serverActor, invite, "invite");

    const addPostActor = spawnStateless(serverActor, addPost, "add_post");
	spawnStateless(addPostActor, receiverCleanCollection, "receivers_clean");
	spawnStateless(addPostActor, sourceCleanCollection, "sources_clean");
    spawnStateless(addPostActor, say, "say");
    spawnStateless(addPostActor, receiverCollection, "receivers");
    spawnStateless(addPostActor, client, "client");

}
