import db from "../../../db";

import {ObjectId} from "promised-mongo";
import {DateTime} from "graphql-scalars";

const { dispatch, query } = require('nact');

export default async function(msg, ctx) {

    try {
        const post = await db.post.insert(msg.input);

        const external_id =msg.input.external_id;
        const external_type = msg.input.external_type;
        const external_system = msg.input.external_system;

        const sourceCollectionActor = ctx.children.get("sources_clean");

        let source =  await query(sourceCollectionActor, {search: {
                external_id: external_id,
                external_type: external_type,
                external_system: external_system,
        }}, 2000);
		
		source = source.length ? source[0] : null;
		
		if (!source || source.is_disabled) {
			dispatch(ctx.sender,   post , ctx.self);
			return;
		}

        const receiverCollectionActor = ctx.children.get("receivers_clean");

        let receivers = [];
		let match = { activate_source_ids: source._id };
		if (!source.is_public) {
			match._id = {$in : source.shared_to_receivers_ids};
		}
        receivers = await query(receiverCollectionActor, {search: match}, 2000);
		console.log(receivers);
        receivers.forEach( async receiver => {
			if (receiver.is_disabled) {
				return;
			}
			delete receiver.admins;
			var sent = false;
			for(let i1 in receiver.themes) {
				let theme = receiver.themes[i1];
				if (!theme.activate) {
					return;
				}
				if (sent) {
					return;
				}
				for(let i2 in theme.keywords) {
					let keyword = theme.keywords[i2];
					if (sent) {
						return;
					}
					if (msg.input.post_text.search(new RegExp(keyword, "i")) > -1) {
						const clientActor = ctx.children.get("client");

						let client = await query(clientActor, {search: {external_system: receiver.external_system}}, 2000);
						console.log("client");
						console.log(client);

						const sayActor = ctx.children.get("say");

						query(sayActor, {
							"api_uri": client.api_uri,
							"client": client,
							"external_id": receiver.external_id,
							"external_type": receiver.external_type,
							"message": source.title + " (" + source.external_system + "):\n" + 
								post.user_first_name + " " + post.user_second_name + ":\n" + 
								msg.input.post_text + "\n\n" + 
								msg.input.external_post_link,
							"slack_team_id": receiver.slack_team_id
						}, 2000);
						sent = true;


					}
				}
			}
        })

        await db.repost.insert({
            post_id: post._id,
            repost_time: new Date().toISOString(),
            source_copy: source,
            receivers_copy: receivers
        });

        dispatch(ctx.sender,   post , ctx.self);

    } catch (e) {
        console.log(e);
    }

}


//    let telegram_post;
// 	let args = msg.input;
//     let db_args = args;
//     delete db_args._id;
//     if(args.id) {
//
//         telegram_post = await db.telegram_post.findAndModify({
//             query: {_id: new ObjectId(args._id)},
//             update: {
//                 $set : db_args
//             }
//         });
//     }
//     else {
//
//         telegram_post = await db.telegram_post.insert(db_args);
//     }
//
//     dispatch(ctx.sender,  telegram_post, ctx.self);
// 	return;
//
//
//

//
//     let source = await db.source.findOne({telegram_chat_id: args.chat_id});
//     if (source) {
//         let config = await db.config.findOne();
//         let themes = await db.theme.find();
//         let words = [];
//
//         for (let n in themes) {
//             let theme = themes[n];
//             for (let n2 in themes[n].keywords) {
//                 let word = themes[n].keywords[n2];
//
//                 if (args.post_text.search(new RegExp(word, "i")) > -1) {
//                     if (source && word) {
//                         let receivers = await db.subscription.find({ themes_ids: new ObjectId(theme._id), sources_ids: new ObjectId(source._id) });
//                         for (let n3 in receivers) {
//                             let receiver = receivers[n3];
//
//                             let telegram_post_link = "https://t.me/c/" +
//                                 args.chat_id.replace(/^\-100/, "") + "/" + args.post_id;
//
//                             var telegram = require('telegram-bot-api');
//
//                             var api = new telegram({
//                                 token: config.telegram_token,
//                             });
//
//                             api.sendMessage({
//                                 chat_id: receiver.telegram_chat_id,
//                                 text: args.chat_title + ":\n" + args.post_text + "\n\n" + telegram_post_link
//                             })
//
//                             //say
//
//
//
//                         }
//                     }
//                 }
//             }
//         }
//     }



