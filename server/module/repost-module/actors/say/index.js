import gql from "graphql-tag";
import jws from "jws";
import {readFileSync} from "fs";

import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import AssertionTokenType from '../../../../token-types/AssertionTokenType';
import createClientKey from '../../../../token-types/createClientKey';
import db from '../../../../db';

import fetch from 'node-fetch'

const say = readFileSync(__dirname + '/graphql/say.graphql', 'utf8');
const say_slack = readFileSync(__dirname + '/graphql/say_slack.graphql', 'utf8');

export default async function(msg, ctx) {
	try {
		let server = (await db.client.find({client_id: 'server'}))[0];
		let tokenType = new AssertionTokenType('', await createClientKey(server));
		tokenType.createToken(server, server);
		const token = jws.sign({
			header: { alg: 'HS256' },
			payload: tokenType.json,
			secret: server.client_secret,
		  });

		const api_uri = msg.api_uri;

		const httpLink = createHttpLink({
			"uri": api_uri,
			fetch: fetch,
			headers: {
				Authorization: "Bearer " + token
			}
		});

		console.log(api_uri);
		console.log(msg);

	// const authLink = setContext((_, { headers }) => {
	//     // get the authentication token from local storage if it exists
	//     // const token = localStorage.getItem('token');
	//     const token = "";
	//     // return the headers to the context so httpLink can read them
	//     // const xxx = base64_encode( login +':'+ password );
	//     //console.log(token);
	//     return {
	//         headers: {
	//             ...headers,
	//             authorization: token ? `Barier ${token}` : "",
	//             // authorization: 'Basic ' + xxx,
	//         }
	//     }
	// });



		const client = new ApolloClient({
			link: ApolloLink.from([
				// authLink,
				httpLink,
			]),
			cache: new InMemoryCache(),
			// defaultHttpLink: false,
			// cache: cache,

			// fetchOptions: {
			//     credentials: 'include'
			// }
		});
		
		let variables = {
				external_type: msg.external_type,
				external_id: msg.external_id,
				message: msg.message
			};
		let mutation = gql`
			${say}
		`;

		if (msg.slack_team_id) {
			mutation = gql`
				${say_slack}
			`;
			variables.slack_team_id = msg.slack_team_id;
		}
		client.mutate({
            mutation: mutation,
			variables: variables
		})
			.then(data => console.log(data))
			.catch(error => console.error(error));
	} catch (e) {
        console.log(e);
    }
}

//					var telegram = require('telegram-bot-api');
//
// 					var api = new telegram({
// 						token: config.telegram_token,
// 					});
//
// 					let telegram_post_link = "https://t.me/c/" +
// 						args.chat_id.replace(/^\-100/, "") + "/" + args.post_id;
// 					api.sendMessage({
// 						chat_id: subscription.telegram_chat_id,
// 						text: args.chat_title + ":\n" + args.post_text + "\n\n" + telegram_post_link
// 					})//say


//say (message: String telegram_chat_id: String): String mutation
