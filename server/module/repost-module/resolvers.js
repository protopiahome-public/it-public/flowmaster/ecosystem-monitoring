import {ObjectId} from "promised-mongo";
import bcrypt from "bcrypt";


import {getMyResourcesFilter} from "./helpers/getMyResources";
import {DateTimeResolver} from "graphql-scalars";

const { query } = require('nact');

module.exports = {
    DateTime: DateTimeResolver,
	Post : {
        __resolveType(post, context, info){
            //			if (post.external_system == 'telegram')
            // 			{
            // 				return 'TelegramPost';
            // 			}
            // 			if (post.external_system == 'vk')
            // 			{
            // 				return 'VkPost';
            // 			}
            // 			if (post.external_system == 'slack')
            // 			{
            // 				return 'SlackPost';
            // 			}
            return 'TelegramPost';
        }
	},
    Query: {

        getConfig: async (obj, args, ctx, info) => {

            const configCollectionActor = ctx.children.get("config");

            let tokens = await query(configCollectionActor, {"type": "tokens"}, 2000);

            return tokens;

        },
		
		showInvite: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;
			const inviteActor = ctx.children.get("invite");

            let invite = await query(inviteActor, { show: { invite_code: args.invite} }, 2000);
			if (!invite) {
				return;
			}
			
            return await query(collectionItemActor, {"type": "receiver",
                    search: {_id: invite.receiver_id}
                },
                2000);


        },

        getIdByLink: async (obj, args, ctx, info) => {

            const linkActor = ctx.children.get("link");

            return await query(linkActor, { search: { link_code: args.link} }, 2000);
        },


        getThemes: async (obj, args, ctx, info) => {

            let currnet_user = ctx.user;

            const themeCollectionActor = await ctx.children.get("themes");

            const themes =  await query(themeCollectionActor, {search : {user_id: currnet_user._id}}, 500);

            return themes;

        },

        getTheme: async (obj, args, ctx, info) => {

            let currnet_user = ctx.user;


            const themeCollectionActor = ctx.children.get("themes");

            const themes =  await query(themeCollectionActor, { search: {id: args.id, user_id: currnet_user._id}}, 2000);

            let theme = themes[0];
            return  theme || {};

        },

        getThemeByTitle: async (obj, args, ctx, info) => {
            let currnet_user = await ctx.user;

            const themeCollectionActor = ctx.children.get("themes");

            const themes =  await query(themeCollectionActor, {search: {_id: args._id, user_id: currnet_user._id}}, 2000);

            let theme = themes[0];
            return  theme || {};
        },

        getThemesByReceiver: async (obj, args, ctx, info) => {
            let currnet_user = await ctx.user;


            const themeCollectionActor = ctx.children.get("themes");

            const themes =  await query(themeCollectionActor, {search: {_id: args._id, user_id: currnet_user._id}}, 2000);

            let theme = themes[0];
            return  theme || {};
        },

        getReceiver: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;

            const receiverCollectionActor = ctx.children.get("receivers");


            const receivers =  await query(receiverCollectionActor, {search: {_id: args.id, user_id: currnet_user._id}}, 2000);
            let receiver = receivers[0];
            return  receiver || {};

        },
        getMyReceivers: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;

            const receiverCollectionActor = ctx.children.get("receivers");


            const receivers =  await query(receiverCollectionActor, {search: {admin_ids: new ObjectId(currnet_user._id), user_id: currnet_user._id}}, 2000);
            return  receivers;

        },
        getReceivers: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;

            const receiverCollectionActor = ctx.children.get("receivers");

            return   await query(receiverCollectionActor, {search: { user_id: currnet_user._id}}, 2000);

        },

        getReceiverByTitle: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "receiver",
                    search: args,
                },
                2000);

        },

        getReceiverByExternal: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "receiver",
                    search: args,
                },
                2000);

        },

        getSource: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;

            const sourceCollectionActor = ctx.children.get("sources");

            const source =  await query(sourceCollectionActor, {search: {_id: args.id , user_id: currnet_user._id}}, 2000);
			
            return source ? source[0] : null;

        },

        getMySources: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;

            const sourceCollectionActor = ctx.children.get("sources");

            const source =  await query(sourceCollectionActor, {search: {admin_ids: new ObjectId(currnet_user._id) , user_id: currnet_user._id}}, 2000);
			
            return source;

        },

        getSources: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;

            const sourceCollectionActor = ctx.children.get("sources");

            return await query(sourceCollectionActor, {search: {user_id: currnet_user._id}}, 2000);
        },

        getSharedSourcesByReceiver: async (obj, args, ctx, info) => {

            let currnet_user = await ctx.user;
			
			const collectionItemActor = ctx.children.get("item");

            const sourceCollectionActor = ctx.children.get("sources");

            return await query(sourceCollectionActor, {search: {user_id: currnet_user._id, shared_to_receivers_ids: new ObjectId(args.receiver_id)}}, 2000);

        },

        getSourceByTitle: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "source",
                    search: args,
                },
                2000);

        },

        getSourceByExternal: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "source",
                    search: args,
                    },
                2000);

        },

        getSourcePostLog: async (obj, args, ctx, info) => {
			let user = await ctx.user;
            const collectionItemActor = ctx.children.get("posts");
			let post = await query(collectionItemActor, {
                    search: {
						_id: args.post_id
					},
                },
                2000);
			console.log(post);
			if (!post) {
				return;
			} else {
				post = post[0];
			}

			const sourceCollectionActor = ctx.children.get("sources");

            let source =  await query(sourceCollectionActor, {search: {
						"external_system": post.external_system,
						"external_id": post.external_id,
						user_id: user._id}}, 2000);
			if (!source) {
				return;
			} else {
				source = source[0];
			}
			
            let result = await query(collectionItemActor, {
                    search: {
						"external_system": source.external_system,
						"external_id": source.external_id,
					},
                },
                2000);
				
			return {
				source: source,
				posts: result
			};

            },
		getMyRepostLog: async (obj, args, ctx, info) => {
			let user = await ctx.user;
            const collectionItemActor = ctx.children.get("reposts");

            let result = await query(collectionItemActor, {
                    search: {
						"receivers_copy.admin_ids": new ObjectId(user._id)
					},
                },
                2000);
				
			return result;

            },

        getReceiverRepostLog: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("reposts");

            return await query(collectionItemActor, {
                    search: {"receivers_copy._id": args.receiver_id},
                },
                2000);
        },


    },
    Mutation:{

        createLinkById: async (obj, args, ctx, info) => {

            const linkActor = ctx.children.get("link");

            return await query(linkActor, { input: { _id: args.id} }, 2000);
        },

        changeConfig: async (obj, args, ctx, info) => {

            const configCollectionActor = ctx.children.get("config");

            return await query(configCollectionActor, {"type": "tokens", input: args.input}, 2000);


        },

        addPost: async (obj, args, ctx, info) => {

            const addPost = ctx.children.get("add_post");

            let post = await query(addPost, {input: args.input}, 2000);
			
			return post;

        },

        changeTheme: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;

            const themeItemActor = ctx.children.get("theme");

            let theme;
			let receiver_id = args.receiver_id;
			
			let receiver = await ctx.db.receiver.find({
				query: {_id: new ObjectId(receiver_id), admin_ids: new ObjectId(current_user._id)}, 
			});
			if (!receiver) {
				return;
			}
			
            if(args._id) {
                theme =  await query(themeItemActor, {
                    search: {_id: args._id},
                    input: args.input
                }, 2000);
            } else {
                theme =  await query(themeItemActor, {
                    input: args.input
                }, 2000);
				await ctx.db.receiver.findAndModify({
					query: {_id: new ObjectId(receiver_id)}, 
					update: {$addToSet: { themes_ids: new ObjectId(theme._id)  }}
				});
			}
			
            return theme;

        },

        addKeyWordToTheme: async (obj, args, ctx, info) => {

            const themeItemActor = ctx.children.get("theme");
			let current_user = await ctx.user;
            return await query(themeItemActor, {
                search: {_id: new ObjectId (args.theme_id), user_id: new ObjectId(current_user._id)},
                full_input: { $addToSet: { keywords: args.keyword } }
            }, 2000);


        },

        removeKeyWordFromTheme: async (obj, args, ctx, info) => {

            const themeItemActor = ctx.children.get("theme");

            return await query(themeItemActor, {
                search: {_id: args.theme_id},
                full_input: { $pull: { keywords: args.keyword } }
            }, 2000);

        },

        activateTheme: async (obj, args, ctx, info) => {
            const themeItemActor = ctx.children.get("theme");

            return await query(themeItemActor, {
                search: {_id: args.theme_id},
                full_input: { $pull: { keywords: args.keyword } }
            }, 2000);
        },

        deactivateTheme: async (obj, args, ctx, info) => {
            const themeItemActor = ctx.children.get("theme");

            return await query(themeItemActor, {
                search: {_id: args.theme_id},
                full_input: { $pull: { keywords: args.keyword } }
            }, 2000);
        },

        copyThemeToReceiver: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;
            const themeItemActor = ctx.children.get("theme");

            const old_theme =  await query(themeItemActor, {search: {_id: args._id}, user_id: current_user._id}, 2000);
            delete old_theme._id;
            if (!old_theme) {
                return;
            }
            let theme = await ctx.db.theme.insert(old_theme);
			
            let receiver_member = await ctx.db.receiver.findAndModify({
				query: {_id: new ObjectId(args.receiver_id), admin_ids: new ObjectId(current_user._id)}, 
				update: {$addToSet: { themes_ids: new ObjectId(theme._id)  }}
			});
            if (!receiver_member) {
                return;
            }



            return theme;

        //                const {collectionItemActor} = require("../../actorSystem");
            //
            //             const current_user = ctx.user;
            //
            //
            //
            //             let theme = await db.theme.find(
            //                 await getMyResourcesFilter(ctx, 'theme',
            //                 {"_id": new ObjectId(args.theme_id) }
            //             )
            //             );
            //             if (!theme) {
            //                 return null;
            //             }
            //
            //             return await query(collectionItemActor, {type: "receiver",
            //                     search: { _id: args._id, admin_ids: new ObjectId(current_user._id)},
            //                     input: {$addToSet: { themes_ids: new ObjectId(args.theme_id) } }},
            //                 2000);

        },

        deleteTheme: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;
            await ctx.db.theme.remove(await getMyResourcesFilter(ctx, 'theme', {_id: new ObjectId(args._id)}));

            return true;

        },

        changeSource: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            const current_user = ctx.user;
			
			let source;

            if(args._id) {
                source = await query(collectionItemActor, {type: "source",
                        search: { _id: args._id, admin_ids: new ObjectId(current_user._id)},
                        input: args.input},
                    2000);
            }else{
				source = await query(collectionItemActor, {type: "source",
					search: {
						external_id: args.input.external_id,
						external_type: args.input.external_type,
						external_system: args.input.external_system,
					}},
                    2000);
				if (source) {
					return source;
				}
                args.input.admin_ids = [new ObjectId(current_user._id)];
                source = await query(collectionItemActor, {type: "source", input: args.input}, 2000);
            }

            return source;


        },

        addAdminSource: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;

            return await query(collectionItemActor, {type: "source",
                    search: { _id: args.id},
                    full_input: { $addToSet: { admin_ids: new ObjectId(args.member_id) } }
                },
                2000);

        },

        removeAdminSource: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;

            return await query(collectionItemActor, {type: "source",
                    search: { _id: args.id},
                    full_input: { $pull: { admin_ids: new ObjectId(args.member_id) } }
                },
                2000);

        },

        shareSource: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;

            return await query(collectionItemActor, {"type": "source",
                    search: {_id: args.source_id, admin_ids: new ObjectId(current_user._id)},
                    full_input: { $addToSet: { shared_to_receivers_ids: new ObjectId(args.receiver_id) } }
                },
                2000);


        },

        //sources и rescivers это две разные но очень схожие по полям коллекции.
        unshareSource: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;

            return await query(collectionItemActor, {"type": "source",
                    search: {_id: args.source_id, admin_ids: new ObjectId(current_user._id)},
                    full_input: { $pull: { shared_to_receivers_ids: new ObjectId(args.receiver_id) } }
                },
                2000);

        },

        createInvite: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;

			let source = await query(collectionItemActor, {"type": "receiver",
                    search: {_id: args.receiver_id, admin_ids: new ObjectId(current_user._id)}
                },
                2000);
			if (source) {
				const inviteActor = ctx.children.get("invite");

				return await query(inviteActor, { input: { receiver_id: args.receiver_id} }, 2000);
			}
        },
		
		deleteInvite: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;
			const inviteActor = ctx.children.get("invite");

            let invite = await query(inviteActor, { show: { invite_code: args.invite} }, 2000);
			if (!invite) {
				return;
			}
			
			let receiver = await query(collectionItemActor, {"type": "receiver",
                    search: {_id: invite.receiver_id, admin_ids: new ObjectId(current_user._id)}
                },
                2000);
			if (receiver) {
				let invite = await query(inviteActor, { "delete": { invite_code: args.invite} }, 2000);
				return true;
			}

        },


        activateInvite: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;
			const inviteActor = ctx.children.get("invite");

            let invite = await query(inviteActor, { search: { invite_code: args.invite} }, 2000);
			if (!invite) {
				return;
			}
			let source = await query(collectionItemActor, {"type": "source",
                    search: {_id: args.source_id, admin_ids: new ObjectId(current_user._id)}
                },
                2000);
			if (!source) {
				return;
			}
			

            await query(collectionItemActor, {"type": "source",
                    search: {_id: args.source_id},
                    full_input: { $addToSet: { shared_to_receivers_ids: new ObjectId(invite.receiver_id) } }
                },
                2000);
            
            await query(collectionItemActor, {type: "receiver",
                search: { _id: invite.receiver_id},
                full_input: {$addToSet: { activate_source_ids: new ObjectId(source._id) } }},
            2000);

            return await query(collectionItemActor, {"type": "receiver",
                    search: {_id: invite.receiver_id},
                },
                2000);

        },

        deleteSource: async (obj, args, ctx, info) => {
			return;
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;

            await ctx.db.source.remove({ _id: args.id, admin_ids: new ObjectId(current_user._id)});
        },


        changeReceiver: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            const current_user = ctx.user;
			
			if (args.input.themes) {
				let new_themes_ids = [];
				for (let theme of args.input.themes) {
					let theme_id = theme._id;
					delete theme._id;
					if (theme_id){
						theme = await query(collectionItemActor, {type: "theme",input: theme, search: {_id: theme_id}}, 2000);
					} else {
						theme = await query(collectionItemActor, {type: "theme",input: theme}, 2000);
					}
					new_themes_ids.push(theme._id.toString());
				}
				if (args._id) {
					let current_receiver = (await query(collectionItemActor, {type: "receiver",
                        search: { _id: args._id, admin_ids: new ObjectId(current_user._id)}}, 2000));
					if (current_receiver) {
						for (let theme of current_receiver.themes_ids) {
							if (!new_themes_ids.includes(theme.toString())) {
								console.log(theme.toString());
								console.log(new_themes_ids);
								console.log("deleted");
								await ctx.db.theme.remove({_id: new ObjectId(theme)});
							}
						}
					}
				}
				for (let theme in new_themes_ids) {
					new_themes_ids[theme] = new ObjectId(new_themes_ids[theme]);
				}
				args.input.themes_ids = new_themes_ids;
				delete args.input.themes;
			}

            let receiver ={};

            if(args._id) {
                receiver = await query(collectionItemActor, {type: "receiver",
                        search: { _id: args._id, admin_ids: new ObjectId(current_user._id)},
                        input: args.input},
                    2000);
            }else{
				receiver = await query(collectionItemActor, {type: "receiver",
					search: {
						external_id: args.external_id,
						external_type: args.external_type,
						external_system: args.external_system,
					}},
                    2000);
				if (receiver) {
					return receiver;
				}
                args.input.admin_ids = [new ObjectId(current_user._id)];
                receiver = await query(collectionItemActor, {type: "receiver",input: args.input}, 2000);
            }

            return receiver;

        },

        addAdminReceiver: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            const current_user = ctx.user;
            return  await query(collectionItemActor, {type: "receiver",
                    search: { _id: args.id},
                    full_input: {$addToSet: { admin_ids: new ObjectId(args.member_id) } }},
                2000);


        },
        removeAdminReceiver: async (obj, args, ctx, info) => {
            const collectionItemActor = ctx.children.get("item");

            const current_user = ctx.user;
            return  await query(collectionItemActor, {type: "receiver",
                    search: { _id: args._id},
                    full_input: {$pull: { admin_ids: new ObjectId(args.member_id) } }},
                2000);

        },
        activateSourceReceiver: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            const current_user = ctx.user;

            return await query(collectionItemActor, {type: "receiver",
                    search: { _id: args.id, admin_ids: new ObjectId(current_user._id)},
                    full_input: {$addToSet: { activate_source_ids: new ObjectId(args.source_id) } }},
                2000);

            // let receiver = await query(collectionItemActor, {"type": "group",
            //         search: {type: "receiver", _id: args.receiver_id, admin_ids: new ObjectId(current_user._id)},
            //         input: {$addToSet: { sources_ids: new ObjectId(args.source_id) } }},
            //     2000);

        },

        deactivateSourceReceiver: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            const current_user = ctx.user;

            return await query(collectionItemActor, {type: "receiver",
                    search: {_id: args.id, admin_ids: new ObjectId(current_user._id)},
                    full_input: {$pull: { activate_source_ids: new ObjectId(args.source_id) } }},
                2000);

            // let receiver = await query(collectionItemActor, {"type": "group",
            //         search: {type: "receiver", _id: args.receiver_id, admin_ids: new ObjectId(current_user._id)},
            //         input: {$pull: { sources_ids: new ObjectId(args.source_id) } }},
            //     2000);

        },

        deleteReceiver: async (obj, args, ctx, info) => {
			return;
            const collectionItemActor = ctx.children.get("item");
            const current_user = ctx.user;
			
			await ctx.db.receiver.remove({ _id: args.id, admin_ids: new ObjectId(current_user._id)});
        },

    }

}