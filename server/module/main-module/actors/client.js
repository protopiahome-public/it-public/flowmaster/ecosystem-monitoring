import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

//Stateless - Сохраняющий свое состояние во внешний источник.

const resource = "client";

export default async function(msg, ctx)
{

    let client = {};

    if(msg._id && msg.input){
        client = db.client.findAndModify({

            query: {_id: new ObjectId(msg._id)},
            update: {
                $set : msg.input
            }
        });

    }else if(msg.input){
        client = db.client.insert(msg.input);
    }else {
        if(msg._id){
            msg._id = new ObjectId(msg._id);
        }

        client = await db.client.findOne(msg);

    }

    dispatch(ctx.sender,   client , ctx.self);


    // await db.client.findOne({clientSecret: authorizationArray[1]});
}