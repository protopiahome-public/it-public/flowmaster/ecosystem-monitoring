import {loginHint, loginHintToken, loginUserCode, idHintToken}from "../authentication";
import authorizeResponse from "../response/authorizeResponse";
import authIDFlow from "../flows/authIDFlow";


import {
    AccessDeniedError,
    InvalidScopeError,
    MissingUserCodeError,
    UnauthorizedClientError
} from "../../errors";

import getClientByAssertion from "../helpers/getClientByAssertion";




export default async function(args, ctx) {

    const {client, key}  = await getClientByAssertion(args.assertion, ctx);

    const scope = args.scope ? args.scope : [];
    //InvalidScopeError
    //AccessDeniedError

    const login_hint_token = args.login_hint_token;
    const id_token_hint = args.id_token_hint;
    const login_hint = args.login_hint;

    const user_code = args.user_code;



    let user;

    if(login_hint_token && user_code) {
        if(!user_code){
            throw new MissingUserCodeError("missing user code");
        }
        user = await loginHintToken(login_hint_token, user_code, key, ctx);

    } else if(login_hint && user_code){
        if(!user_code){
            throw new MissingUserCodeError("missing user code");
        }
        user = await loginHint(login_hint, user_code, ctx);
    }
    else if (user_code){

        user = await loginUserCode(user_code, client, ctx);
    }
    else if(id_token_hint ){
        user = await idHintToken(id_token_hint, key, ctx);
    }


    const interval = 25;
    const expires_in = Date.now() + interval;

    const auth_req_id = await authIDFlow(client, user, scope, ctx);

    return authorizeResponse(auth_req_id, expires_in, interval, args.state);

}