import { GraphQLModule } from '@graphql-modules/core';
import {existsSync, readdirSync, readFileSync, statSync} from "fs";
const path = require('path');

//https://www.apollographql.com/docs/apollo-server/features/errors/

//https://medium.com/the-guild/graphql-modules-feature-based-graphql-modules-at-scale-2d7b2b0da6d

//https://medium.com/the-guild/authentication-and-authorization-in-graphql-and-how-graphql-modules-can-help-fadc1ee5b0c2

//https://graphql-modules.com/docs/introduction/implement-server

export default function(ctx) {

    let imports = [];
    const dir = __dirname + "/module";
    const files = readdirSync(dir);
    for (const file of files) {
        const stat = statSync(path.join(dir, file));
        if (stat.isDirectory()){

            if (existsSync(path.join(dir, file, "/index.js"))){
                const module = require(path.join(dir, file, "/index.js"));
                imports.push(module.default(ctx));
            }

        }
    }

    return new GraphQLModule({
        name: 'app',
        imports: imports
    });

}

